import time
import requests
from src.helper import CsvHelper
from src.config import timeout, user_count, url


def time_is_over(start_time):
    return time.time() - start_time > timeout


def main():
    start_time = time.time()
    user_data = [f'{user_id}, None' for user_id in range(1, user_count + 1)]
    csv_helper = CsvHelper()

    while 1:
        try:
            for user_id in range(1, user_count + 1):
                id, level = requests.get(url + f'{user_id}').json().values()
                user_data[id - 1] = f'{id},{level}'

                if time_is_over(start_time):
                    print('Запись в файл!')
                    start_time = time.time()
                    csv_helper.save_file(user_data)

                print(user_id)
        except Exception as e:
            print(e)


if __name__ == '__main__':
    main()
