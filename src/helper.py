import datetime
import os

dir = './files'
date_format = '%Y-%m-%d_%H_%M_%S'


class CsvHelper:
    def __init__(self):
        self.file_name = 'levels-{}.csv'

    def save_file(self, data):
        name = self.file_name.format(
            datetime.datetime.today().strftime(date_format)
        )
        with open(name, 'w') as f:
            t = '\n'.join(map(str, data))
            return f.write(t)
